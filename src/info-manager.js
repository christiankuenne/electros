const os = require('os');
const version = require('./../package.json').version;

class InfoManager {

    getSections() {
        return [
            { id: 'os', name: 'Operating System' },
            { id: 'user', name: 'User' },
            { id: 'processor', name: 'Processor' },
            { id: 'memory', name: 'Memory' },
            { id: 'network', name: 'Network' }
        ];
    }

    getOsInfos(section) {
        switch (section) {
            case 'os':
                return [
                    { name: 'Operating System', value: os.type() + " " + os.release() },
                    { name: 'Host Name', value: os.hostname() },
                    { name: 'Temp Directory', value: os.tmpdir() }
                ];
            case 'user':
                return [
                    { name: 'User Name', value: os.userInfo().username },
                    { name: 'User Id', value: os.userInfo().uid },
                    { name: 'Group Id', value: os.userInfo().gid },
                    { name: 'Shell', value: os.userInfo().shell },
                    { name: 'Home Directory', value: os.userInfo().homedir }
                ];
            case 'processor':
                return [
                    { name: 'Architecture', value: os.arch() },
                    { name: 'Endianness', value: os.endianness() },
                    { name: 'CPU Model', value: os.cpus().length + "x " + os.cpus()[0].model }
                ];
            case 'memory':
                return [
                    { name: 'Total Memory', value: (os.totalmem() / (1024 * 1024)).toLocaleString() + ' MB' },
                    { name: 'Free Memory', value: (os.freemem() / (1024 * 1024)).toLocaleString() + ' MB' }
                ];
            case 'network':
                var interfaces = []
                Object.keys(os.networkInterfaces()).forEach(function(key) {
                    interfaces.push({ name: key, value: '' });
                    for (var i in os.networkInterfaces()[key]) {
                        var current = os.networkInterfaces()[key][i];
                        interfaces.push({ name: current.family + ' Address', value: current.address });
                        interfaces.push({ name: current.family + ' Net Mask', value: current.netmask });
                    }
                    interfaces.push({ name: 'Mac Address', value: os.networkInterfaces()[key][0].mac });
                    interfaces.push({ name: '---', value: '' });
                })
                return interfaces;
        }
    }

    getVersion() {
        return version;
    }
}