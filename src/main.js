const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

app.on('ready', () => {
    let win = new BrowserWindow({width:600, height:400});
    win.loadURL(`file://${__dirname}/index.html`);
    //win.webContents.openDevTools();
});