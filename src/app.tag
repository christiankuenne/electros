<app>

  <div class="navbar">
    <ul>
      <li each={ sections }><a class="{ selected: id == selectedId }" href="#{ id}" onclick="{click(id)}">{ name }</a></li>
    </ul>
  </div>

  <div class="infopanel">
    <table>
      <tr each={ infos }>
        <th>{ name }</th>
        <td>{ value }</td>
      </tr>
    </table>
  </div>

  <div id="footer">electrOS { version }</div>

  <script>
    var self = this;
    var infoManager = new InfoManager();
    self.version = infoManager.getVersion();
    self.sections = infoManager.getSections();
    self.selectedId = self.sections[0].id;

    self.click = function (id) {
      return function () {
        self.selectedId = id;
        showDetails();
      }
    };

    var showDetails = function() {
      self.infos = infoManager.getOsInfos(self.selectedId);
    };

    showDetails(self.selectedId);    
  </script>

</app>