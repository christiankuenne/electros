# electrOS #

A small example app with [Electron](https://electron.atom.io/) and [riot.js](http://riotjs.com/): it displays a couple of operating system information from [Node.js OS](https://nodejs.org/api/os.html) module.

### Dependencies ###

* Node.js
* Electron
* riot.js

### Installation ###

`git clone https://bitbucket.org/christiankuenne/electros`

`cd electros`

`npm install`

`npm start`

2017 Christian Künne